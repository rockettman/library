

class TileBag(object):
    """Holds remaining tiles and randomly returns tiles when asked"""
    def __init__(self):
        self.tiles = []


class TileRack(object):
    """Holds 7 tiles"""
    def __init__(self):
        self.tiles = []


class Tile(object):
    """Tile for placement on board"""
    def __init__(self):
        self.letter


class Player(object):
    """A player has a tile rack and takes turns placing words"""
    def __init__(self):
        self.name
        self.tile_rack
    
    def select_move(self):
        pass


class ComputerPlayer(Player):
    pass


class Game(object):
    """A game has a board, a tile bag, and 2-4 players"""
    def __init__(self):
        self.board
        self.tile_bag
        self.players = []
    
    def play(self):
        pass
    
    def take_turn(self, player):
        pass
    
    def is_game_over(self):
        pass
    
    def print_final_results(self):
        pass
